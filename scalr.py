#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import socket
import commands
import logging

try:
	import pxssh
	import pexpect
except:
	pass

import urllib2

#Решение стандартными средствами Python (built-in ) без использования сторонних библиотек

#Если требуется убрать дебаг сообщения, следует поменять флаг на logging.INFO 
logging.basicConfig(level = logging.DEBUG)

#местоположение для загруженного файла 
SERVER_DIRECTORY = '/tmp/'

class ServerInfo(object):
	"""docstring for ServerInfo"""

	def getLoadavg(self):
		return os.getloadavg()	

	def getCoreCount(self):
		#Try to know count of CPU cores
		# if Python 2.6+
	    try:
	        import multiprocessing
	        return multiprocessing.cpu_count()
	    except (ImportError,NotImplementedError):
	        pass

	    # POSIX
	    try:
	        import os
	        res = int(os.sysconf('SC_NPROCESSORS_ONLN'))
	        if res > 0:
	            return res
	    except (AttributeError,ValueError):
	        pass        

	def  printFullInfo(self):
		"""Print required information about server
		"""
		print """-------------"""
		print """ Load average: %s, %s, %s """ % self.getLoadavg() 
		print """ Cpu cores: %s """ % self.getCoreCount()
		print """Block device names: \n %s""" % commands.getoutput('lsblk -o NAME | tail -n+2')
		print """-------------"""
		print """Mount points of filesystems: \n %s""" % commands.getoutput("mount -l | awk '{print $1 ,  $3}'")
		print """-------------"""
		print """The amount of available disk space at /root  partition: %s""" % commands.getoutput("df --output=avail / | tail -n+2")
		print """-------------"""
		print """Installed packages in OS: \n %s""" % commands.getoutput("dpkg --get-selections | awk '{print $1}'")		
		print """-------------"""
	
		        
	def removeMyself(self, scriptName = __file__):
		try:
			os.remove(scriptName)
		except Exception as ex:
			logging.error(ex)
	
	def __del__(self):
		self.removeMyself()

       

class DedicatedSever(object):
	__hostname = 'ec2-54-91-115-126.compute-1.amazonaws.com'
	__login =  'root' 
	__password = 'ofu5Ahb4'
	__connection = None
	__sample = '.compute-1.internal'
	__child = None


	def loadScript(self, scriptName=__file__):
		sftpCommands = ['scp', '-P 22', os.path.abspath(scriptName),  """%s@%s:%s%s""" 
			% (self.__login, self.__hostname, SERVER_DIRECTORY, scriptName)]
		logging.info("Uploading...")

		#result of script loading
		out = ''
		try:
			self.__child = pexpect.spawn( ' '.join(sftpCommands))
			ind = self.__child.expect([r"""password:""", r"""yes/no"""])
			if ind == 0:
				self.__child.sendline(self.__password)
			elif ind == 1:
				self.__child.sendline("yes")
				self.__child.expect("password:")
				self.__child.sendline(self.__password)
			out = self.__child.read()		
			logging.debug(out)
		except Exception as ex:
			logging.error("SCP load myself to dedicated fail ") 
			logging.error(ex)	 
			sys.exit(1)
		finally:
			if self.__child is not None:
				self.__child.close() 	

		if out.find('100%') != -1:
			return True
		return False		
			

	def executeScript(self, scriptName = __file__):
		try:
			self.__connection = pxssh.pxssh()
			if not self.__connection.login(self.__hostname, self.__login, self.__password):
				raise Exception('Cannot login by SSH')
			else:
				self.__connection.sendline('python %s' % ''.join([SERVER_DIRECTORY, scriptName]))		
				self.__connection.prompt()
				print self.__connection.before

				self.__connection.logout()
		except Exception as ex:
			logging.error("Fail SSH connection to DedicatedSever \n%s" %	 ex)
			sys.exit(1)		
	
	def isAlreadyMyself(self):
		domain_name = socket.getfqdn()				
		if domain_name.find(self.__sample) != -1:
			return True
		return False	


def checkInternetConnection():
	try:
		response = urllib2.urlopen('http://google.com',timeout=1)
		logging.debug('Connection OK')
		return True
	except urllib2.URLError as err: 
		pass
	return False


def main():
	dServer = DedicatedSever()

	#dServer.executeScript()
	if not dServer.isAlreadyMyself() :
		logging.debug("I am original server")

		if not checkInternetConnection():
			exit(1)

		#1 step loading semselves to destination server 
		#connect as SFTP(SCP) and transfer this file 
		if dServer.loadScript():
			#2 step execute this file on dedicated server
			dServer.executeScript()
		return
	
	logging.debug("I am dedicated server")
	#3 step  output of required information	about server
	sInfo = ServerInfo()
	sInfo.printFullInfo()


if __name__ == '__main__':
	main()
